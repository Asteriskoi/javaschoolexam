package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int buff = inputNumbers.size();
        int i =1;

        if(buff<3) throw new CannotBuildPyramidException();
        if(inputNumbers.contains(null))  throw new CannotBuildPyramidException();

        while (buff>0){
            buff=buff-i;
            i++;
        }
        i--;
        if(buff!=0){
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);

        int rows = i;
        int columns =i+i-1;
        int[][] result = new int[rows][columns];
        for(i=0; i<rows; i++){
            for(int j=0; j<columns; j++){
                result[i][j]=0;
            }
        }
        buff = inputNumbers.size()-1;
        int tail = columns-1;
        int head = 0;



        for(i=1;i<=rows;i++){
            int border = tail;
            while (tail>=head){
                result[rows-i][tail]=inputNumbers.get(buff);
                buff--;
                tail-=2;
            }
            head++;
            tail=--border;
        }
        return result;
    }


}
