package com.tsystems.javaschool.tasks.calculator;


public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        double result = 0.0;
        try {
            String noParentheses = statement;
            while (noParentheses.contains("(")) {
                if (!statement.contains(")")) {
                    return null;
                }
                noParentheses = parentheses(noParentheses);
            }
            String[] workArray;
            workArray = noParentheses.split("\\+");
            double value;
            for (int i = 0; i < workArray.length; i++) {
                if (workArray[i].contains("-")) {
                    value = minus(workArray[i]);
                } else if (workArray[i].contains("*")) {
                    value = multiply(workArray[i], true);
                } else if (workArray[i].contains("/")) {
                    value = decrease(workArray[i], true);
                } else {
                    value = Double.parseDouble(workArray[i]);
                }
                if (i == 0) {
                    result = value;
                } else {
                    result += value;
                }
            }
        }catch (Exception ex){
            return null;
        }

        if (result % 1 == 0) {
                return Integer.toString((int) result);
            } else{
            return Double.toString(result);
        }
    }

    String parentheses(String input){
        int begin = input.indexOf("(");
        int end = input.indexOf(")");
        String result ="";
        result= input.substring(0,begin);
        String workString="";
        workString = input.substring(begin+1,end);
        result=result.concat(evaluate(workString));
        result = result.concat(input.substring(end+1, input.length()));
        return result;
    }
    double minus(String input){
        String negativeDecrease = "/-";
        String negativeMultiply = "*-";
        String buffString = input;
        String noNegativeD="";
        while (buffString.contains(negativeDecrease)){
            noNegativeD="";
            int begin = buffString.indexOf(negativeDecrease);
            noNegativeD = noNegativeD.concat(buffString.substring(0,begin));
            noNegativeD = noNegativeD.concat("|");
            noNegativeD = noNegativeD.concat(buffString.substring(begin+2,buffString.length()));
            buffString=noNegativeD;
        }
        while (buffString.contains(negativeMultiply)){
            noNegativeD="";
            int begin = buffString.indexOf(negativeMultiply);
            noNegativeD = noNegativeD.concat(buffString.substring(0,begin));
            noNegativeD = noNegativeD.concat("!");
            noNegativeD = noNegativeD.concat(buffString.substring(begin+2,buffString.length()));
            buffString=noNegativeD;
        }
        String[] workArray;
        workArray = buffString.split("\\-");
        double result = 0.0;
        double value = 0.0;
        for(int i = 0; i<workArray.length;i++){
            if(workArray[i].contains("*")){
                value = multiply(workArray[i], true);
            }else if(workArray[i].contains("!")){
                value = -multiply(workArray[i],false);
            }
            else if(workArray[i].contains("/")){
                value = decrease(workArray[i],true);
            }else if(workArray[i].contains("|")){
                value = -decrease(workArray[i],false);
            } else{
                value = Double.parseDouble(workArray[i]);
            }

            if(i==0){
                result=value;
            }else {
                result-=value;
            }
        }
        return result;
    }

    double multiply(String input, boolean sign){
        String[] workArray;
        if(sign) {
            workArray = input.split("\\*");
        }
        else {
            workArray = input.split("\\!");
        }
        double result = 0.0;
        double value;
        for(int i = 0; i<workArray.length;i++){
            if(workArray[i].contains("/")) {
                value = decrease(workArray[i],true);
            } else if(workArray[i].contains("|")){
                value = -decrease(workArray[i],false);
            }
            else{
                value = Double.parseDouble(workArray[i]);
            }
            if(i==0){
                result=value;
            }else {
                result*=value;
            }
        }
        return result;
    }

    double decrease(String input, boolean sign){
        String[] workArray;
        if(sign) {
            workArray = input.split("\\/");
        }else {
            workArray = input.split("\\|");
        }
        double result = Double.parseDouble(workArray[0]);
        for(int i = 1; i<workArray.length;i++){
                result /= Double.parseDouble(workArray[i]);
                if(Double.toString(result).equals("Infinity")){
                    throw new ArithmeticException();
                }
        }
        return result;
    }

}

